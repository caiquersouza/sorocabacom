<?php

namespace Database\Seeders;

use App\Models\HomePage;
use Illuminate\Database\Seeder;

class HomePageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Homepage::create(array(
            'nome' => 'SuperGiantGames',
            'frase' => 'Olha, o que quer que você esteja pensando, me faça um favor, não solte.',
            'destaque' => 'teste',
            'formulario' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'img' => 'default-background.png',
        ));
    }
}
