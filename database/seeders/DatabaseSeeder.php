<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\HomePage;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UserSeeder::class);
        $this->call(HomePageSeeder::class);

        $this->command->info('Usuário Administrador e Conteúdos default da Home Page Criados com sucesso!');
    }
}
