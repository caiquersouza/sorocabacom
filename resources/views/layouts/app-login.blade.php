@include('layouts.head')

<body class="login">

    <div id="app">
        <main>
            @yield('content')
        </main>
    </div>

    @include('layouts.footer')

</body>

</html>