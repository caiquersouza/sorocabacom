<div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container">
        <div class="row">
            <div class="col-6 text-right align-self-center">
                <img src="{{ asset('img/imagem-3.png') }}" alt="" width="50px" height="50px">
            </div>
            <div class="col-6 text-left align-self-center pl-0">
                <span class="title-site">@isset($home_page->formulario){{ $home_page->nome }}@endisset</span>
            </div>
        </div>
    </div>
</div>