@include('layouts.head')

<body>

    <div id="app">
        <main>
            @yield('content')
        </main>
    </div>

    @include('layouts.footer')

</body>

</html>