<!-- Plugins dropzone -->
<script src="{{ asset('asset/vendor/dropzone/dropzone.min.js') }}"></script>
<script src="{{ asset('asset/vendor/dropify/dropify.min.js') }}"></script>
<script src="{{ asset('asset/vendor/pages/form-fileuploads.init.js') }}"></script>


<!-- plugins owlcarousel -->
<script src="{{ asset('libs/owlcarousel/owl.carousel.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>


<!-- Js tema painel -->
<script src="{{ asset('asset/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('asset/js/sb-admin-2.min.js') }}"></script>

<!-- Validação form -->
<script src="{{ asset('asset/vendor/parsleyjs/parsley.min.js') }}"></script>

<!-- plugins DataTable -->
<script src="{{ asset('asset/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('asset/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('asset/js/demo/datatables-demo.js') }}"></script>