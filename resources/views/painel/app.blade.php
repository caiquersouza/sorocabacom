@include('layouts.head')

<body id="page-top">

    <div id="wrapper">

        @include('painel.sidebar')

        <div id="content-wrapper" class="d-flex flex-column">

            <div id="content">

                @include('painel.nav')

                <div class="container-fluid">

                    @include('painel.flash-message')

                    @yield('content')

                </div>

            </div>

            <footer class="sticky-footer bg-white no-print">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Sorocabacom - {{ date("Y") }}</span>
                    </div>
                </div>
            </footer>

        </div>

    </div>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up pt-2"></i>
    </a>

    @include('layouts.footer')

</body>