@extends('painel.app')
@section('content')

<div class="d-sm-flex align-items-center justify-content-between my-4">
    <h5 class="mb-0 text-gray-800">Personagens</h5>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-success">Lista de Personagens</h6>
        <div class="dropdown no-arrow">
            <a href="{{ route('novo-personagem') }}" class="btn btn-sm btn-primary btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-user-plus"></i>
                </span>
                <span class="text">NOVO</span>
            </a>
        </div>
    </div>
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NOME</th>
                        <th>IMAGEM</th>
                        <th>DESCRIÇÂO</th>
                        <th>STATUS</th>
                        <th>ALTERAÇÃO</th>
                        <th>AÇÃO</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>NOME</th>
                        <th>IMAGEM</th>
                        <th>DESCRIÇÂO</th>
                        <th>STATUS</th>
                        <th>DATA</th>
                        <th>AÇÃO</th>
                    </tr>
                </tfoot>
                <tbody>

                    @forelse ($personagens as $prs)

                    @if ($prs->status == 0)
                    @php($status = 'ATIVO')
                    @php($alerta = 'success')
                    @else
                    @php($status = 'SUSPENSO')
                    @php($alerta = 'danger')
                    @endif

                    <tr>
                        <td>{{ $prs->id }}</td>
                        <td>{{ $prs->nome }}</td>
                        <td class="text-center img-personagem">
                            <img src="img/personagens/{{ $prs->img }}" class="rounded-circle avatar-sm" />
                        </td>
                        <td>{{ $prs->descricao }}</td>
                        <td class="text-center">
                            <span class="badge badge-pill badge-{{ $alerta }}">
                                {{ $status }}
                            </span>
                        </td>
                        <td>{{ date('d-m-Y H:i', strtotime($prs->updated_at)) }}</td>
                        <td class="text-center" style="white-space: nowrap">
                            <a href="{{ route('edit-personagem', ['id'=>$prs->id]) }}" class="btn btn-warning btn-circle btn-sm">
                                <i class="fas fa-user-edit"></i>
                            </a>
                            <a href="{{ route('delete-personagem', ['id'=>$prs->id]) }}" class="btn btn-danger btn-circle btn-sm">
                                <i class="fas fa-trash-alt"></i>
                            </a>

                            <!-- <a data-toggle="modal" data-target="#exampleModal" data-nome="{{ $prs->nome }}" data-id="{{ $prs->id }}" class="btn btn-danger btn-circle btn-sm">
                                <i class="fas fa-trash-alt"></i>
                            </a> -->
                        </td>
                    </tr>

                    @empty

                    @endforelse

                </tbody>
            </table>
        </div>




    </div>
</div>

@endsection