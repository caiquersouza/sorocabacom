  <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar">

      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
          <div class="sidebar-brand-icon">
              <img src="{{ asset('img/imagem-3.png') }}" alt="" width="70px" height="70px">
          </div>
          <div class="sidebar-brand-text mx-3">
              SUPERGIANTGAME
          </div>
      </a>

      <hr class="sidebar-divider my-0">

      <li class="nav-item">
          <a class="nav-link" href="{{ route('home') }}">
              <i class="fas fa-home text-white"></i>
              <span>Home</span>
          </a>
      </li>

      <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
              <i class="fas fa-street-view text-white"></i>
              <span>Personagens</span>
          </a>
          <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
              <div class="bg-white py-2 collapse-inner rounded">
                  <a class="collapse-item" href="{{ route('personagens') }}">Lista</a>
                  <a class="collapse-item" href="{{ route('novo-personagem') }}">Novo</a>
              </div>
          </div>
      </li>

      <hr class="sidebar-divider d-none d-md-block">

      <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

  </ul>