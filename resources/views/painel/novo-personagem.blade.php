@extends('painel.app')
@section('content')

<div class="d-sm-flex align-items-center justify-content-between my-4">
    <h5 class="mb-0 text-gray-800">Novo Personagem</h5>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-success">Personagem</h6>
        <div class="dropdown no-arrow">
            <a href="{{ route('personagens') }}" class="btn btn-sm btn-primary btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-users"></i>
                </span>
                <span class="text">PERSONAGENS</span>
            </a>
        </div>
    </div>
    <div class="card-body">

        <form method="POST" action="{{ route('store-personagem') }}" enctype="multipart/form-data" data-parsley-validate="">
            {!! csrf_field() !!}

            <div class="form-row">

                <div class="col-md-6">
                    <div class="form-group col-md-12">
                        <label>Imagem do Personagem</label>
                        <input type="file" class="dropify" data-max-file-size="2M" name="img" parsley-trigger="change" class="form-control" required accept="image/png, image/jpeg">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">

                        <div class="form-group col-md-12">
                            <label>Nome</label>
                            <input type="text" name="nome" class="form-control" required maxlength="100" placeholder="Informe o nome do personagem">
                        </div>

                        <div class="form-group col-md-12">
                            <label>Descrição</label>
                            <textarea type="text" name="descricao" class="form-control" required maxlength="500" rows="5" placeholder="Informe a descrição do personagem"></textarea>
                        </div>

                    </div>
                </div>

                <div class="col-12 text-right border-top">
                    <button type="submit" class="btn btn-success btn-icon-split btn-sm mt-3">
                        <span class="icon text-white-50">
                            <i class="fas fa-save"></i>
                        </span>
                        <span class="text">SALVAR</span>
                    </button>
                </div>
            </div>

        </form>

    </div>
</div>

@endsection