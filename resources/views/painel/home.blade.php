@extends('painel.app')
@section('content')

<div class="d-sm-flex align-items-center justify-content-between my-4">
    <h5 class="mb-0 text-gray-800">Home Page</h5>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-success">Editar Conteúdos</h6>
        <div class="dropdown no-arrow">
            <span class="ultima-alteracao">
                <strong>Ultima alteração</strong> @isset($home_page->nome){{ date('d-m-Y - H:i', strtotime($home_page->updated_at)) }}@endisset
            </span>
        </div>
    </div>
    <div class="card-body">

        <form method="POST" action="{{ route('update-home') }}" enctype="multipart/form-data" data-parsley-validate="">
            {!! csrf_field() !!}

            <div class="form-row">

                <div class="col-md-6">
                    <div class="form-group col-md-12">
                        <label>Imagem do Backgroud</label>
                        <input type="file" class="dropify" data-default-file="../../img/background/{{ $home_page->img }}" data-max-file-size="2M" name="img" parsley-trigger="change" class="form-control" accept="image/png, image/jpeg">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">

                        <div class="form-group col-md-12">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="nome" maxlength="100" value="@isset($home_page->nome){{ $home_page->nome }}@endisset" required placeholder="Informe o nome do Jogo">
                        </div>

                        <div class="form-group col-md-12">
                            <label>Frase</label>
                            <input type="text" class="form-control" name="frase" maxlength="100" value="@isset($home_page->frase){{ $home_page->frase }}@endisset" required placeholder="Informe uma frase">
                        </div>

                        <div class="form-group col-md-12">
                            <label>Destaque</label>
                            <textarea type="text" class="form-control" name="destaque" maxlength="500" rows="5" required placeholder="Informe o texto destaque do tema">@isset($home_page->destaque){{ $home_page->destaque }}@endisset</textarea>
                        </div>

                        <div class="form-group col-md-12">
                            <label>Formulário</label>
                            <textarea type="text" class="form-control" name="formulario" maxlength="500" rows="5" required placeholder="Informe a descrição do formulário">@isset($home_page->formulario){{ $home_page->formulario }}@endisset</textarea>
                        </div>

                    </div>
                </div>

                <div class="col-12 text-right border-top">
                    <button type="submit" class="btn btn-success btn-icon-split btn-sm mt-3">
                        <span class="icon text-white-50">
                            <i class="fas fa-save"></i>
                        </span>
                        <span class="text">SALVAR</span>
                    </button>
                </div>
            </div>

        </form>

    </div>
</div>

@endsection