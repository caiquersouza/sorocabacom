@extends('layouts.app')
@section('content')

@include('layouts.top-bar')

<section id="home" style="background-image: url('../../img/background/{{ $home_page->img }}');">

    <div class="container frase">
        <div class="row d-flex justify-content-center">
            <div class="col-6">
                <p class="text-white">
                    "@isset($home_page->frase){{ $home_page->frase }}@endisset"
                </p>
            </div>
        </div>
    </div>

</section>

<section id="personagens" class="py-5">
    <div class="container">

        <div class="py-5">

            <div class="owl-carousel owl-theme">

                @forelse ($personagens as $prs)

                <div class="item d-flex justify-content-center">
                    <div class="card">
                        <img class="card-img-top img-fluid" src="img/personagens/{{ $prs->img }}" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text text-justify">{{ $prs->descricao }}</p>
                        </div>
                    </div>
                </div>

                @empty

                <div class="container">
                    <div class="col-6 text-center">
                        <h5>Nenhum personagem cadastrado!</h5>
                    </div>
                </div>

                @endforelse

            </div>
        </div>

    </div>
</section>

<section id="contato">
    <div class="container">

        <div class="row d-flex justify-content-center">
            <div class="col-md-8 col-sm-12  bg-white formulario rounded">

                <div class="p-sm-5">

                    <h4 class="text-center mb-4 mt-4">FORMULÁRIO</h4>
                    <p class="text-justify">
                        @isset($home_page->formulario){{ $home_page->formulario }}@endisset
                    </p>

                    <form action="" class="p-sm-5">
                        <div class="row mb-4">
                            <div class="col">
                                <input type="text" class="form-control rounded-0" placeholder="Nome">
                            </div>
                            <div class="col">
                                <input type="email" class="form-control rounded-0" placeholder="Email">
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col">
                                <textarea type="text" class="form-control rounded-0" placeholder="Mensagem" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-6">
                                <button type="submit" class="btn btn-block rounded-0">ENVIAR</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>

    </div>
</section>

<section id="footer"></section>

@endsection