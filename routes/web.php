<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LPController;
use App\Http\Controllers\HomePageController;
use App\Http\Controllers\PersonagensController;


Route::get('/', [LPController::class, 'index']);

Auth::routes(['register' => false, 'reset' => false, 'verify' => true,]);

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Route Home Page
Route::group(['prefix' => 'home', 'middleware' => 'auth', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('/', [HomePageController::class, 'home'])->name('home');
    Route::post('/update', [HomePageController::class, 'update'])->name('update-home');
});



//Route personagens
Route::group(['prefix' => 'personagens', 'middleware' => 'auth', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('/', [PersonagensController::class, 'personagens'])->name('personagens');
    Route::get('/novo', [PersonagensController::class, 'novo'])->name('novo-personagem');
    Route::post('/store', [PersonagensController::class, 'store'])->name('store-personagem');
    Route::get('/edit/{id}', [PersonagensController::class, 'edit'])->name('edit-personagem');
    Route::post('/update/{id}', [PersonagensController::class, 'update'])->name('update-personagem');
    Route::get('/delete/{id}', [PersonagensController::class, 'delete'])->name('delete-personagem');
    // Route::post('/delete', [PersonagensController::class, 'delete'])->name('delete-personagem');
});
