//Owl init
$('.owl-carousel').owlCarousel({
    // loop: true,
    margin: 0,
    nav: true,
    // autoplay: true,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        750: {
            items: 1
        },
        1000: {
            items: 3
        }
    }
});