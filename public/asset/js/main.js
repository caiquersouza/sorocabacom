//Form Pesquisa Avançada
function buscar() {
    var inicio = $("input[name='inicio']").val()
    var fim = $("input[name='fim']").val()

    if (fim != '') {
        if (fim < inicio) {
            $("#erro-data").text("Data Final não pode ser menor que Data Inicial!").show().fadeOut(15000);
            event.preventDefault();
        } else {
            return;
        }
    }
}