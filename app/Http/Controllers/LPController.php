<?php

namespace App\Http\Controllers;

use App\Models\HomePage;
use App\Models\Personagens;
use Illuminate\Http\Request;

class LPController extends Controller
{

    public function index()
    {

        $home_page = HomePage::latest()->first();
        $personagens = Personagens::where('status', 0)->get();

        return view('welcome', compact('home_page', 'personagens'));
    }
}
