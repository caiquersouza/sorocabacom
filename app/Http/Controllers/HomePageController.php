<?php

namespace App\Http\Controllers;

use App\Models\HomePage;
use Illuminate\Http\Request;

class HomePageController extends Controller
{

    public function home()
    {

        $home_page = HomePage::latest()->first();
        return view('painel.home', compact('home_page'));
    }

    public function update(Request $request)
    {

        //Verifica se tem upload
        if ($request->hasFile('img')) {

            $image = $request->file('img');
            $name_imagem = time() . '.' . $image->getClientOriginalExtension();

            //salvando a imagem nessa pasta
            $destinationPath = public_path('/img/background');
            $image->move($destinationPath, $name_imagem);
        } else {

            $imagem_anterio = HomePage::latest()->first();
            $name_imagem = $imagem_anterio->img;
        }

        //Insert na tabela
        HomePage::latest()->first()->update([
            'nome' => $request->nome,
            'frase' => $request->frase,
            'destaque' => $request->destaque,
            'formulario' => $request->formulario,
            'img' => $name_imagem
        ]);

        return redirect()
            ->action('App\Http\Controllers\HomePageController@home')
            ->with('info', 'Conteúdo alterado com sucesso!');
    }
};
