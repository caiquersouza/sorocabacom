<?php

namespace App\Http\Controllers;

use App\Models\Personagens;
use Illuminate\Http\Request;

class PersonagensController extends Controller
{
    public function personagens()
    {

        $personagens = Personagens::all();
        return view('painel.personagens', compact('personagens'));
    }

    public function novo()
    {

        return view('painel.novo-personagem');
    }

    public function store(Request $request)
    {


        $image = $request->file('img');
        $name_imagem = time() . '.' . $image->getClientOriginalExtension();

        //salvando a imagem nessa pasta
        $destinationPath = public_path('/img/personagens');
        $image->move($destinationPath, $name_imagem);


        //Insert na tabela
        Personagens::create([
            'nome' => $request->nome,
            'descricao' => $request->descricao,
            'img' => $name_imagem,
            'status' => 0
        ]);

        return redirect()->back()
            ->with('success', 'Personagem cadastrado com sucesso!');
    }

    public function edit($id)
    {

        $personagem = Personagens::find($id);
        return view('painel.edit-personagem', compact('personagem'));
    }

    public function update(Request $request, $id)
    {

        //Verifica se tem upload
        if ($request->hasFile('img')) {

            $image = $request->file('img');
            $name_imagem = time() . '.' . $image->getClientOriginalExtension();

            //salvando a imagem nessa pasta
            $destinationPath = public_path('/img/personagens');
            $image->move($destinationPath, $name_imagem);
        } else {

            $imagem_anterio = Personagens::latest()->first();
            $name_imagem = $imagem_anterio->img;
        }

        //Verifica o status
        $status = (!isset($request['status'])) ? 0 : 1;

        //Insert na tabela
        Personagens::find($id)->update([
            'nome' => $request->nome,
            'descricao' => $request->descricao,
            'img' => $name_imagem,
            'status' => $status
        ]);

        return redirect()->action('App\Http\Controllers\PersonagensController@personagens')
            ->with('info', 'Personagem alterado com sucesso!');
    }

    public function delete($id)
    {

        $personagem = Personagens::find($id);
        $personagem->delete();

        return redirect()->action('App\Http\Controllers\PersonagensController@personagens')
            ->with('warning', 'Personagem deletado com sucesso!');
    }
}
